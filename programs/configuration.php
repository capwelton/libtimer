<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

class LibTimer_ConfigurationPage
{
	private function forceForm()
	{
	    $W = bab_Widgets();
	    
	    $form = $W->Form();
	    $form->setName('force')
	    ->addClass('widget-bordered')
	    ->addClass('BabLoginMenuBackground')
	    ->addClass('libtimer-form');
	    $form->setHiddenValue('tg', bab_rp('tg'));
	    
	    $event = $W->Select();
	    $event->addOption('Monthly', 'Monthly');
	    $event->addOption('Weekly', 'Weekly');
	    $event->addOption('Daily', 'Daily');
	    $event->addOption('Hourly', 'Hourly');
	    $event->addOption('Every30Min', 'Every30Min');
	    $event->addOption('Every5Min', 'Every5Min');
	    
	    $form->addItem($W->LabelledWidget(libtimer_translate('Force event execution on'), $event, 'event'));
	    
	    $form->addItem($W->SubmitButton()
				->setLabel(libtimer_translate('Execute')));
	    return $form;
	}
	
	private function getForm()
	{
		$W = bab_Widgets();
		
		$form = $W->Form();
		$form->setName('configuration')
		    ->addClass('widget-bordered')
		    ->addClass('BabLoginMenuBackground')
		    ->addClass('libtimer-form');
		$form->setHiddenValue('tg', bab_rp('tg'));
		
		$radioSet = $W->RadioSet();
		$radioSet->setName('method');
		$radioSet->addOption('url', sprintf(libtimer_translate('Execute scheduled tasks when this link is called : %s'), $GLOBALS['babUrlScript'].'?tg=addon/LibTimer/call'));
		$radioSet->addOption('page', libtimer_translate('Try to execute scheduled tasks while users browse the site'));
		$radioSet->addOption('deamon', libtimer_translate('Try to execute scheduled tasks in background (linux server only)'));
		
		$form->addItem($radioSet);
		
		$noptions = array(
			'' => libtimer_translate('Do not use nice')
		);
		
		for($i=-20; $i<19; $i++) { $noptions[$i] = $i; }
		$noptions[-20] = libtimer_translate('-20: most favorable to the process');
		$noptions[10] = libtimer_translate('10: default');
		$noptions[19] = libtimer_translate('19: least favorable to the process');
		
		$coptions = array(
			'' => libtimer_translate('Do not use ionice'),
			0 => libtimer_translate('0: none'), 
			1 => libtimer_translate('1: realtime'),
			2 => libtimer_translate('2: best-effort'),
			3 => libtimer_translate('3: idle')
		);
		
		$form->addItem(
			$h = $W->HBoxItems(
				$W->LabelledWidget(libtimer_translate('nice'), $W->Select()->setOptions($noptions), 'nice'),
				$W->LabelledWidget(libtimer_translate('ionice'), $W->Select()->setOptions($coptions), 'ionice')
			)->setHorizontalSpacing(2,'em')
		);
		
		$radioSet->setAssociatedDisplayable($h, array('deamon'));
		
		
		$form->addItem(
			$W->SubmitButton()
				->setLabel(libtimer_translate('Save'))
		);
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibTimer/');
		
		$method = $registry->getValue('method', 'page');
		$nice = $registry->getValue('nice');
		$ionice = $registry->getValue('ionice');
		
		$form->setValues(array('configuration' => array(
			'method' => $method,
			'nice' => $nice,
			'ionice' => $ionice
		)));
		
		return $form;
	}
	
	
	private function getLogTable()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		$table = $W->TableView()->addClass('libtimer-log');
		$table->addSection('Header');
		$table->setCurrentSection('Header');
		
		$table->addItem($W->Label(libtimer_translate('Module'))		, 0,0);
		$table->addItem($W->Label(libtimer_translate('Description')), 0,1);
		$table->addItem($W->Label(libtimer_translate('Date'))		, 0,2);
		$table->addItem($W->Label(libtimer_translate('Event'))		, 0,3);
		
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM libtimer_log ORDER BY created DESC LIMIT 0,100');
		$row = 1;
		$table->addSection('Body');
		$table->setCurrentSection('Body');
		
		while ($arr = $babDB->db_fetch_assoc($res))
		{
			$table->addItem($W->Label($arr['module'])		, $row,0);
			$table->addItem($W->Label($arr['description'])	, $row,1);
			$table->addItem($W->Label(bab_shortDate(bab_mktime($arr['created'])))		, $row,2);
			$table->addItem($W->Label($arr['event'])		, $row,3);
			$row++;
		}
		
		return $table;
	}
	
	
	
	
	private function getLastHit()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		$frame = $W->Frame()->addClass('libtimer-lasthit');
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibTimer/');
		$lasthit = $registry->getValue('lasthit');
		
		if (null === $lasthit)
		{
			$frame->addItem($W->Label(libtimer_translate('No hit recorded, the timer does not work yet')));
		} else {
			$t = bab_mktime($lasthit);
			$d = time() - $t;
			
			if ($d < (5*60))
			{
				$frame->addItem($W->Label(libtimer_translate('The timer has been called in the last 5 minutes, good configuration')));
			} else {
				$frame->addItem($W->Label(sprintf(libtimer_translate('The last call to timer was at %s, timer should be called every 5 minutes'), bab_shortDate($t))));
			}
		}
		
		
		
		return $frame;
	}
	
	
	public function display()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		$page = $W->BabPage();
		
		
		
		$url = bab_url::get_request('tg');
		$url->idx = 'form';
		$page->addItemMenu('form', libtimer_translate('Configuration'), $url->toString());
		$url->idx = 'log';
		$page->addItemMenu('log', libtimer_translate('Scheduled tasks log'), $url->toString());
		$idx = bab_rp('idx', 'form');
		$page->setCurrentItemMenu($idx);
		
		$page->addStyleSheet($GLOBALS['babInstallPath'].'styles/addons/LibTimer/main.css');
		
		switch($idx)
		{
			case 'form':
				$page->setTitle(libtimer_translate('Scheduled tasks options'));
				$page->addItem($this->getForm());
				$page->addItem($this->getLastHit());
				$page->addItem($this->forceForm());
				break;
			case 'log':
				$page->setTitle(libtimer_translate('Scheduled tasks log'));
				$page->addItem($this->getLogTable());
				break;
			
		}
		$page->displayHtml();
	}
	
	
	public function save($configuration)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibTimer/');
		
		$registry->setKeyValue('method', $configuration['method']);
		
		if ('' !== $configuration['nice'])
		{
			$registry->setKeyValue('nice', (int) $configuration['nice']);
		} else {
			$registry->removeKey('nice');
		}
		
		if ('' !== $configuration['ionice'])
		{
			$registry->setKeyValue('ionice', (int) $configuration['ionice']);
		} else {
			$registry->removeKey('ionice');
		}
	}
	
	
	public function force($force)
	{
	    $timer = bab_functionality::get('Timer');
	    $method = 'new'.$force['event'];
	    $eventObject = $timer->$method();
	    
	    require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
	    bab_fireEvent($eventObject);
	}
}


if (!bab_isUserAdministrator())
{
	return;
}

$page = new LibTimer_ConfigurationPage;

if (!empty($_POST))
{
    bab_requireSaveMethod();
    if (isset($_POST['configuration'])) {
	    $page->save(bab_pp('configuration'));
    }
    
    if (isset($_POST['force'])) {
        $page->force(bab_pp('force'));
    }
}

$page->display();