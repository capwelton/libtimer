;<?php/*

[general]
name                        ="LibTimer"
version                     ="1.1.4"
delete                      =1
db_prefix                   ="libtimer_"
encoding                    ="UTF-8"
mysql_character_set_database="latin1,utf8"
description                 ="Library for scheduled Tasks"
description.fr              ="Librairie partagée permettant le traitement de tâches planifiées"
long_description.fr         ="README.md"
author                      ="Paul de Rosanbo (Cantico)"
ov_version                  ="8.4.92"
php_version                 ="5.1.0"
mysql_version               ="4.1.2"
addon_access_control        ="0"
icon                        ="Timer.png"
configuration_page          ="configuration"
tags                        ="library,default"

[addons]
widgets		="0.2.23"

;*/?>
