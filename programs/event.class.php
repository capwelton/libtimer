<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';


abstract class LibTimer_event extends bab_event
{
    /**
     * Flag used to redirect log to bab_debug instead of logging to database
     */
    public $debugLog = false;
    
    
	/**
	 * Add log string to log table
	 * @param	string	$module			Module name
	 * @param	string	$description	string to record
	 * 
	 * @return LibTimer_event
	 */
	public function log($module, $description)
	{
		if ('' === $module || '' === $description)
		{
			throw new ErrorException('Missing module or description');
		}
		
		if ($this->debugLog) {
		    bab_debug($description, DBG_TRACE, get_class($this).' '.$module);
		    return;
		}
		
		
		global $babDB;
		
		$babDB->db_query('INSERT INTO libtimer_log (module, description, created, event) 
			VALUES (
				'.$babDB->quote($module).', 
				'.$babDB->quote($description).', 
				NOW(), 
				'.$babDB->quote($this->getName()).'
			)'
		);
		
		// auto delete log entries older than 3 months
		
		$babDB->db_query('DELETE FROM libtimer_log WHERE ADDDATE(created, INTERVAL 3 MONTH) < NOW()');
		
		return $this;
	}

	
	/**
	 * 
	 * @return string
	 */
	public function getName()
	{
		$classname = get_class($this);
		return substr($classname, strlen(__CLASS__));
	}
}


class LibTimer_eventMonthly extends LibTimer_event
{
	
}

class LibTimer_eventWeekly extends LibTimer_event
{
	
}

class LibTimer_eventDaily extends LibTimer_event
{
	
}

class LibTimer_eventHourly extends LibTimer_event
{
	
}

class LibTimer_eventEvery30Min extends LibTimer_event
{
	
}

class LibTimer_eventEvery5Min extends LibTimer_event
{
	
}