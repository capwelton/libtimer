## Planificateur de tâches ##

Bibliothèque pour exécuter des tâches planifiées du type envoi de mail, synchronisation active directory, ...
Il existe trois fonctionnements différents :

* Sur certains systèmes linux, les tâches planifiées sont exécutées automatiquement à l'intervalle spécifié.
* Sur Windows, il est conseillé d'enregistrer un appel à la page d'exécution des tâches planifiés pour arriver au même fonctionnement.
* Si aucune des deux méthodes n'a été configurée, les tâches sont exécutées par les utilisateurs lors de leur interaction avec le portail. Cela étant, l'intervalle entre deux lancements d'une même tâche planifiée n'est alors pas nécessairement respectée.